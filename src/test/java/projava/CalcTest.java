package projava;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CalcTest {
    @Test
    public void testAdd() {
        assertEquals(6, new Calc().add(2, 4), "2 + 2 = 6");
    }
}
