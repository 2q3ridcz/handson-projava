package projava;

public class Calc {
    /**
     * 2つの数値を足し算する
     * @param a 足し算する数値の 1つ目
     * @param b 足し算する数値の 2つ目
     * @return 足し算した結果の数値
     */
    public int add(int a, int b) {
        return a + b;
    }
}
