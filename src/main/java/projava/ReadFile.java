package projava;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class ReadFile {
    public static void main(String[] args) {
        var p = Path.of("test.txt");
        String s;
        try {
            s = Files.readString(p);
        } catch (NoSuchFileException e) {
            System.out.println("ファイルが見つかりません: " + e.getFile());
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        System.out.println(s);
    }
}
