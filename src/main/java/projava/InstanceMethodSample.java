package projava;

public class InstanceMethodSample {
    record Student(String name, int englishScore, int mathScore) {
        int average() {
            return (this.englishScore() + this.mathScore()) / 2;
        }

        void showResult() {
            System.out.println("%sさんの平均点は%d点です".formatted(this.name, this.average()));
        }

        public int maxScore() {
            return Math.max(this.englishScore(), this.mathScore());
        }
    }

    public static void main(String[] args) {
        var kis = new Student("kis", 60, 80);
        var a = average(kis);
        System.out.println("平均点は%d点です".formatted(a));

        var b = kis.average();
        System.out.println("平均点は%d点です".formatted(b));

        kis.showResult();

        int max = kis.maxScore();
        System.out.println("最高点は%d点です".formatted(max));
    }

    static int average(Student s) {
        return (s.englishScore + s.mathScore) / 2;
    }
}
