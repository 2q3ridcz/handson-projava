# handson-projava

書籍「プロになるJava」のハンズオンのメモ。

## 書籍について

[プロになるJava-仕事で必要なプログラミングの知識がゼロから身につく最高の指南書](https://books.rakuten.co.jp/rb/17004835/)

自分にとってとてもいい本だった。  
これまで Java 勉強したいと思いつつ、

- Java はどのバージョンがいい？
- どの JDK がいい？
- Maven と Gradle はどっちがいい？

などなど勉強の前段階の悩みが邪魔で、なかなか踏み出せなかった。  

Web の情報は鮮度にばらつきがあるが、この本は 2022年03月発売で鮮度が高く、とりあえずこの本の言うとおりにやってみようと思うことができた。(今でも通用するプラクティスだと信じることができた。)

本に掲載されているコードを GitHub でも公開してくれている。

[GitHub - projava17/examples](https://github.com/projava17/examples)

## メモ

本の章立てに沿って。

### 第1部 Javaを始める準備
#### 第1章 Java ってなんだろう
#### 第2章 開発環境の準備と最初の一歩

##### 2.1 Oracle JDK のインストール

https://www.oracle.com/java/technologies/downloads/

> 2021年に Oracle JDK は再び用途を問わず無料で利用できるようになりました

知らなかった！ Oracle JDK 有償化で、 Open JDK を探し回った記憶がある…

本では Oracle JDK の Java 17 のインストーラーを薦めている。  
ただし自分の PC には元々 Java 8 を設定済みで、今後も Java 8 を使う必要がある。  

ということで、 Oracle JDK の Java 17 の Compressed Archive をダウンロードし、解凍して java フォルダに格納。環境変数 JAVA_HOME をここに向けるよう変更した。この本の学習時間以外は、 JAVA_HOME を元の Java 8 に向けることを想定。

```
C:\java\java-1.8.0-openjdk    <- 元々 JAVA_HOME に設定していた
C:\java\jdk-17.0.5    <- JAVA_HOME をこっちに変更
```

※注： 元々 jdk-17.0.4.1 と記載していたが、[脆弱性が公表された](https://www.ipa.go.jp/security/ciadr/vul/20221019-jre.html)ため変更。使っている場合はアップデートすること！！  

環境変数の設定方法をいつも忘れてしまう。以下のサイトを参考に設定した。

[Java | PATHの設定及び環境変数JAVA_HOMEの設定](https://www.javadrive.jp/start/install/index4.html)

蛇足。思い出したことの備忘。 Java 8 は下記サイトの 1.8.0 からダウンロード可能。

[GitHub - ojdkbuild/ojdkbuild: Community builds using source code from OpenJDK project](https://github.com/ojdkbuild/ojdkbuild)

##### 2.2 IntelliJ IDEA のインストールと設定

自分の PC は VSCode (Visual Studio Code) がインストール済みなので、 IntelliJ IDEA はインストールしない。  
過去の hands-on でもこんな風に指示に従わなかったために困ったことがあった。今回はどうかな…？

##### 2.3 最初のプログラムを書いてみよう

##### 2.3.1 プロジェクトの作成

VSCode での Maven プロジェクト作成方法を記載する。

1. Ctrl + Shift + P でコマンドパレットを開く。「Java: Create Java Project」を選択。   
    ![create-maven-project_01.png](./docs/img/create-maven-project/create-maven-project_01.png)
2. 「Maven」を選択。  
    ![create-maven-project_02.png](./docs/img/create-maven-project/create-maven-project_02.png)
3. 「maven-archetype-quickstart」を選択。  
    ![create-maven-project_03.png](./docs/img/create-maven-project/create-maven-project_03.png)
4. 「1.4」を選択。  
    ![create-maven-project_04.png](./docs/img/create-maven-project/create-maven-project_04.png)
5. 「jp.gihyo.projava」を入力して Enter。  
    ![create-maven-project_05.png](./docs/img/create-maven-project/create-maven-project_05.png)
6. 「projava」を入力して Enter。  
    ![create-maven-project_06.png](./docs/img/create-maven-project/create-maven-project_06.png)
7. 保存先を選択して「Select Destination Folder」を押下。  
    ![create-maven-project_07.png](./docs/img/create-maven-project/create-maven-project_07.png)
8. Terminalで「Define value for property 'version' 1.0-SNAPSHOT: :」を求められるのでそのまま Enter。  
    ![create-maven-project_08.png](./docs/img/create-maven-project/create-maven-project_08.png)
9. 「Y」を入力して Enter。  
    ![create-maven-project_09.png](./docs/img/create-maven-project/create-maven-project_09.png)
10. 「Open」を押下すると…  
    ![create-maven-project_10.png](./docs/img/create-maven-project/create-maven-project_10.png)
11. Maven プロジェクトの 新しい VSCode Window が開く。  
    ![create-maven-project_11.png](./docs/img/create-maven-project/create-maven-project_11.png)
12. これで終わり…だと思ったが、このままだと Java7 を使う設定になっている。 pom.xml の下記 2 パラメータを「17」に変更して保存。
    - maven.compiler.source
    - maven.compiler.target
    ![create-maven-project_12.png](./docs/img/create-maven-project/create-maven-project_12.png)
13. pom.xml の変更を適用するため「Yes」を押下。  
    ![create-maven-project_13.png](./docs/img/create-maven-project/create-maven-project_13.png)


これと、 git を使う場合は .gitignore を追加すれば OK。

[gitignore/Maven.gitignore at main · github/gitignore · GitHub](https://github.com/github/gitignore/blob/main/Maven.gitignore)

あとは、 VSCode の EXPLORER から Hello.java というファイルを作れば、ファイルを開いて Hello クラスのガラを作ってくれる。(クラスかインターフェースか他のなにかかを選択可能。) そしてクラスの中で「main」と入力して Tab を押下すればすぐにコーディングに移れる。

```
public class Hello {
    public static void main(String[] args) {
        
    }
}
```

### 第2部 Javaの基本
#### 第3章 値と計算

##### 3.1 JShell の起動

JShell めっちゃ便利。

Jshell を終了するときは ```/exit``` だよ！ (忘れがち)

#### 第4章 変数と型
#### 第5章 標準API

##### 5.2 BigDecimal

Coboler として Java で一番気になるものの一つが丸め誤差。それを回避するために BigDecimal は最重要。この本で初めて使ったが、使いにくい。でも大事。 BigDecimal でも + や - などの算術演算子を使えるようにしてくれたらいいのに。

BigDecimal 生成時には数値を文字列として引き渡す癖を付けると失敗しにくそう。

```Java
bigDecimalValue = new BigDecimal("12345678901234567890)
```

#### 第6章 SwingによるGUI

##### 6.1.1 Swing

Java に GUI 部品ライブラリがデフォルトで存在するとは知らなかった。何かに活用できるかな？

##### 6.3.3 IntelliJ IDEA を使わずに実行する

IDE を使わずに Java を実行するところで少し躓き。  
本にコマンドプロンプトから実行すると書いてあったところを PowerShell から実行したらエラー発生。

```PowerShell
PS C:\projava> java -Dfile.encoding=utf-8 .\src\main\java\projava\SampleForm.java
エラー: メイン・クラス.encoding=utf-8を検出およびロードできませんでした
原因: java.lang.ClassNotFoundException: /encoding=utf-8
```

ちょっと調べたら、このサイトを発見。

[PowerShellでencoding指定してJava動かしたら失敗した話 | さくっとLog](https://saktt-sw.com/java-encoding-exec-error)

PowerShell あるあるらしい。サイトに記載の「ダブルクォーテーションで括る」で解決。

```PowerShell
java "-Dfile.encoding=utf-8" .\src\main\java\projava\SampleForm.java
```

ちなみに、コマンドプロンプトでは本のコマンドでも「ダブルクォーテーションで括る」でも実行可能だった。

### 第3部 Javaの文法
#### 第7章 条件分岐

##### 7.1.3 オブジェクトが等しいかどうかの比較

おいおい、嘘だと言ってくれ… 今後絶対間違えるわ…

```Java
jshell> var str = "TEST".toLowerCase()
str ==> "test"

jshell> str == "test"
$34 ==> false
```

> 変数 str が「test」という文字列を保持しているのに、 == 演算子での「test」との比較が false になってしまいました。文字列などオブジェクトの == 演算子による比較は、「同じオブジェクトであるか」という比較であって「同じ値であるか」という比較ではないためです。
> オブジェクトの示す値が等しいかどうかを判定するには equals メソッドを使います。

```Java
jshell> str.equals("test")
$35 ==> true
```

だとしたら、この例の少し前に実行した

```Java
jshell> "test" == "test"
$32 ==> true
```

も false となるべきでは…？ 何が違う…？

> イメージがつかめるまでは難しいと思うので、最初は「オブジェクトの比較は == ではなく equals」と覚えておきましょう。

お前はここで躓くだろうなぁ？ と見透かされたようなコメントが続く。とにかく、下記 2点を心に刻む。

1. オブジェクトの比較は equals。
2. String型 はオブジェクトである。忘れがちだけど。  
    int型のような基本型 (プリミティブ型) とは異なる。

#### 第8章 データ構造

List 、 ArrayList 、 配列、 record 、 Map 、 HashMap など、頻出しそうなやつらが登場。

List は immutable 、 ArrayList は mutable。

List / ArrayList と配列の使い分けはよくわからん。とりあえず List / ArrayList 使えばいい印象。(機能的には。処理性能とか非機能的なメリデメがあるのかもしれない。)

record は dataclass と認識。

Map は immutable 、 HashMap は mutable 。

#### 第9章 繰り返し
#### 第10章 データ構造の処理

Stream 楽しい。

#### 第11章 メソッド
### 第4部 高度なプログラミング
#### 第12章 入出力と例外

これより前でも本で頻出してたけど、 IntelliJ IDEA のメニューすごい。  
そして、 VSCode のメニューもすごい。  
import文や throws IOException とかも自分で書く必要まったくない。  
VSCode は IntelliJ IDEA でできること全てできるわけではないけど、結構いいとこ行ってる印象。  

そして、 Server と Client 作るの楽しい。

#### 第13章 処理の難しさの段階
#### 第14章 クラスとインタフェース

##### 14.3 ラムダ式と関数型インタフェース

何に必要なものなのかよくわからなかった。  
時間があれば戻ってきてもう一度読む。

#### 第15章 継承
### 第5部 ツールと開発技法
#### 第16章 ビルドツールとMaven

> ビルドツールとしてデファクトスタンダードとなっており、この本でも採用している Maven について詳しく説明します。

ビルドツールを勉強してみたいと思いつつ、いつも Maven と Gradle のどちらにすべきか迷っていた。本が決め打ちしてくれると迷いがなくてはかどる。

#### 第17章 Javadocとドキュメンテーション

VSCode でも、ソースコードで「/**」と打つと Javadoc コメントを動的に生成してくれる。

だけどあれ？ mvn javadoc:javadoc がエラーになる…？

```PowerShell
> mvn javadoc:javadoc
mvn : 用語 'mvn' は、コマンドレット、関数、スクリプト ファイル、または操作可能なプログラムの名前として認識されません。名前が正しく記述されていることを確認し、パスが含まれている場合はそのパスが正しいことを確認してから、再試行してください。
```

実行可能な Maven が見つからないことが原因。後述の JUnitの章(第18章) で Spring Boot プロジェクトから mvnw.cmd などをコピーした後で、下記コマンドを実行すると Javadoc を生成できた。

```PowerShell
.\mvnw.cmd javadoc:javadoc
```

Javadoc の生成先は ./target/site/apidocs 。

#### 第18章 JUnitとテストの自動化

VSCode でも、 対象の Class を右クリックして「Go to test」を押下するとテストクラスのガラを動的に生成してくれる。テストのセットアップがまだであれば、セットアップもしてくれる。

JUnit4 がセットアップされたので、本の内容を転記して JUnit5 に変更した。

```xml
  <dependencies>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <version>RELEASE</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

本にはないが、 Maven の test を試してみたら 自分の PC ではエラーが発生した。対処方法を載せておく。

1. 左側の MAVEN > projava > Lifecycle > test の三角ボタンを押下したら、実行可能な Maven が見つからないとエラーになった。  
    ![mvn-test-failed_01.png](./docs/img/mvn-test-failed/mvn-test-failed_01.png)
2. 別の Spring Boot プロジェクトにあった mvnw.cmd などのファイルをこっちのプロジェクトにコピーして再実行。成功。  
    ![mvn-test-failed_02.png](./docs/img/mvn-test-failed/mvn-test-failed_02.png)

実は本の順序どおりに進めておらず、先に 第6部を進めて Spring Boot プロジェクトを作っていた。そっちのプロジェクトで自動作成されていた mvnw.cmd などのファイルをこっちのプロジェクトにコピーすると、 test できるようになった。(Maven をダウンロードして PATH を通すのが正攻法なんだろうけど。)

```
C:.
│  mvnw
│  mvnw.cmd
│
├─.mvn
│  └─wrapper
│          maven-wrapper.jar
│          maven-wrapper.properties
│
```

さて、Test は成功したのだが、実は途中で Warning が発生していた。

```
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for jp.gihyo.projava:projava:jar:1.0-SNAPSHOT
[WARNING] 'dependencies.dependency.version' for org.junit.jupiter:junit-jupiter:jar is either LATEST or RELEASE (both of them are 
being deprecated) @ line 25, column 16
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] ----------------------< jp.gihyo.projava:projava >----------------------
[INFO] Building projava 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
```

JUnit5 の Version を RELEASE にしたのがよくなかった模様。Maven のサイトで調べたら 5.9.1 が最新だったので、 pom.xml の JUnit5 の Version をそれに変更。 Warning 発生しなくなった。

https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter

```xml
  <dependencies>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <version>5.9.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

#### 第19章 IntelliJ IDEAを使いこなす
#### 第20章 バージョン管理とGit
### 第6部 Webアプリケーション開発
#### 第21章 Spring BootでWebアプリケーションを作ってみる

この章の成果物は [handson-projava-tasklist](https://gitlab.com/2q3ridcz/handson-projava-tasklist) で別管理。

##### 21.2.2 Webアプリケーション用のプロジェクトを作る

VSCode での Spring Boot プロジェクト作成方法を記載する。(本では [Sprint Initializr](https://start.spring.io/) でプロジェクトを用意している。)

1. Ctrl + Shift + P でコマンドパレットを開く。「Java: Create Java Project」を選択。   
    ![create-spring-boot-project_01.png](./docs/img/create-spring-boot-project/create-spring-boot-project_01.png)
2. 「Spring Boot」を選択。  
    ![create-spring-boot-project_02.png](./docs/img/create-spring-boot-project/create-spring-boot-project_02.png)
3. ここでワーニング。「Spring Initializr Java Support」という Extension をインストールする必要があるとのこと。「Install」を押下。  
    ![create-spring-boot-project_03.png](./docs/img/create-spring-boot-project/create-spring-boot-project_03.png)
    ![create-spring-boot-project_04.png](./docs/img/create-spring-boot-project/create-spring-boot-project_04.png)
4. 再び手順 1 と 2 をやり直す。すると今度は下記の画面が表示される。「Maven Project」を選択。(ここら辺は本に記載の設定に従っている。)
    ![create-spring-boot-project_05.png](./docs/img/create-spring-boot-project/create-spring-boot-project_05.png)
5. 「2.7.4」を選択。本に「2.6.3、または「SNAPSHOT」や「M1」などが付いていない最新バージョン」と書いてあるため。  
    ![create-spring-boot-project_06.png](./docs/img/create-spring-boot-project/create-spring-boot-project_06.png)
6. 「Java」を選択。  
    ![create-spring-boot-project_07.png](./docs/img/create-spring-boot-project/create-spring-boot-project_07.png)
7. 「jp.gihyo.projava」を入力して Enter。  
    ![create-spring-boot-project_08.png](./docs/img/create-spring-boot-project/create-spring-boot-project_08.png)
8. 「tasklist」を入力して Enter。  
    ![create-spring-boot-project_09.png](./docs/img/create-spring-boot-project/create-spring-boot-project_09.png)
9. 「Jar」を選択。  
    ![create-spring-boot-project_10.png](./docs/img/create-spring-boot-project/create-spring-boot-project_10.png)
10. 「17」を選択。  
    ![create-spring-boot-project_11.png](./docs/img/create-spring-boot-project/create-spring-boot-project_11.png)
11. 「Spring Web」を選択して Enter。  
    ![create-spring-boot-project_12.png](./docs/img/create-spring-boot-project/create-spring-boot-project_12.png)
12. 保存先を選択して「Generate into this folder」を押下。  
    ![create-spring-boot-project_13.png](./docs/img/create-spring-boot-project/create-spring-boot-project_13.png)
13. 「Open」を押下すると…  
    ![create-spring-boot-project_14.png](./docs/img/create-spring-boot-project/create-spring-boot-project_14.png)
14. Spring Boot プロジェクトの 新しい VSCode Window が開く。
    ![create-spring-boot-project_15.png](./docs/img/create-spring-boot-project/create-spring-boot-project_15.png)

ちなみに、途中「Spring Web」を選択した画面では、初期表示されているもの以外にも色々選択できそう。今後は「Spring Batch」も試してみたい。

![create-spring-boot-project_91.png](./docs/img/create-spring-boot-project/create-spring-boot-project_91.png)

もう一つ蛇足。Spring Boot プロジェクトで pom.xml を開くと、 Dependency Analytics のインストールを薦められる。脆弱な依存を検出してくれるのは嬉しいと思い「Install」を押下したが…

![create-spring-boot-project_16.png](./docs/img/create-spring-boot-project/create-spring-boot-project_16.png)

新しい Extension が増えた素振りはなかった。本来はこいつ (Dependency Analitics) がインストールされるはずなんだと思うんだけど…必要に応じて後日調査することにする。

![create-spring-boot-project_17.png](./docs/img/create-spring-boot-project/create-spring-boot-project_17.png)


「JDK と実行構成の設定を行う」は飛ばした。 VSCode だと必要なさそう。

「プロジェクトを実行する」にて、ウイルスソフト Avast 絡みの問題発生。

1. TasklistApplication.java を開き、右上の三角を押下してプロジェクトを実行する  
    ![run-spring-boot-project_00.png](./docs/img/run-spring-boot-project/run-spring-boot-project_00.png)
2. Avast が java.exe を脅威としてブロック。  
    ![run-spring-boot-project_01.png](./docs/img/run-spring-boot-project/run-spring-boot-project_01.png)
3. 暫く放っておいたら、検疫に移動するための再起動を促された。
    ![run-spring-boot-project_02.png](./docs/img/run-spring-boot-project/run-spring-boot-project_02.png)

で、調べてみたが、 Avast が Java を誤検知するのはあるあるらしい。

[Javaのダウンロード・ファイルがウィルスに感染している可能性はありますか。](https://www.java.com/ja/download/help/virus_ja.html)

> 解決策
> 
> Javaソフトウェアのすべてのバージョンは、開発の各ステージで、および公開される前に、複数のアンチウィルス製品を使用してスキャンされます。ソフトウエアにウィルスがないことが入念に確認されています。

Java 公式のこの記述で信用してもいいけど、念のため…

[誤検知？AvastでIDP.Genericのウイルスが検出された時の対処法](https://itojisan.xyz/trouble/17226/)

> - 対処1: Avastのウイルスの定義ファイルをアップデートする
> - 対処2: Javaを最新版にアップデートする
> - 対処3: オンラインスキャンを利用する

こんな感じで、対処方法が続く。「対処3: オンラインスキャンを利用する」に以下の記載あり。対処1 と 2をやっても誤検出されるケースがあるとのこと。

> Avastの定義ファイルを更新しても、IDP.Genericのウィルスが誤検出されるケースがあります。
> 
> そのため、明らかに信頼できる場所からダウンロードしたファイルである場合は、念のため他のセキュリティツールを使ったスキャンも試してみて下さい。

『トレンドマイクロのオンラインスキャンのページ』へのリンクを貼ってくれているが、ここから辿るのは少し怖い…ってことで Bing で [トレンドマイクロのページ](https://www.trendmicro.com/) を検索し、個人のお客さま > 製品 > 無料パソコンセキュリティ診断サービス と辿って [無料で簡単ウイルスチェック！トレンドマイクロ オンラインスキャン｜トレンドマイクロ - 個人のお客さま向けセキュリティ対策](https://www.trendmicro.com/ja_jp/forHome/products/onlinescan.html) に到達。さっきのページのリンク先と同じだった。(疑ってごめん…)

トレンドマイクロ オンラインスキャンを実行したら不正プログラムは検出されなかった。あーよかった。

##### 21.3.4 タスク管理アプリケーションをコマンドラインから起動する

「実行可能な Jar ファイルの作成方法」の VSCode でのやり方。

1. 左下の MAVEN から tasklist > Lifecycle > package と辿り、三角 (「Run」) を押下。  
    ![export-spring-boot-jar_01.png](./docs/img/export-spring-boot-jar/export-spring-boot-jar_01.png)
2. 「TasklistApplication」を選択。  
    ![export-spring-boot-jar_02.png](./docs/img/export-spring-boot-jar/export-spring-boot-jar_02.png)

```PowerShell
java -jar .\target\tasklist-0.0.1-SNAPSHOT.jar
```

で実行可能！

#### 第22章 Webアプリケーションにデータベースを組み込む

##### 22.3.5 HomeController クラスを修正する

HomeController クラスを修正して H2データベースに対応したぞ！  
実行！  
ブラウザからアクセス！　…できないっ！  

![spring-boot-schema-sql-not-working_01.png](./docs/img/spring-boot-schema-sql-not-working/spring-boot-schema-sql-not-working_01.png)

Web検索したら、同じ本の同じところで引っかかった人の記事を発見。

[【Spring Boot】Spring JDBCでschema.sqlを使ったテーブル作成ができない場合の対処 - 未経験35歳から始めるプログラミング](https://35programming.com/spring-boot-schema-sql-create)

このページに記載のとおり、 ./src/main/resources/application.properties に下記 1文を追記したらうまくいった。

```Java
spring.sql.init.mode=always
```

ありがとうセンパイ:heart_eyes:
